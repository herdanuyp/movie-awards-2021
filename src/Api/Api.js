const API_URL=process.env.REACT_APP_BACKEND_API || 'localhost:8080';

const api = {
  async getBallotData() {
    return fetch(`${API_URL}/api/getBallotData`).then(res => res.json()).catch((error) => null)
  }
};

export default api;