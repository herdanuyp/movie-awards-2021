import './App.css';
import Ballot from './Components/Ballot/Ballot';

function App() {
  // Feel free to remove the contents of the header tag to make more room for your code
  return (
    <div className="bg-page-background text-page-font">
      <Ballot />
    </div>
  );
}

export default App;
