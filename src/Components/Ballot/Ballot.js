import React, { useState, useEffect } from 'react';

import api from '../../Api/Api'
import ErrorModal from '../Modal/Error';
import SuccessModal from '../Modal/Success';
import Card from '../Card/Card';
import Banner from '../Navbar/Banner';
import Title from '../Navbar/Title';
import DefaultButton from '../Button/Default';

const Ballot = () => {
  const [ballotData, setBallotData] = useState({});
  const [selectedNomineeList, setSelectedNomineeList] = useState([]);
  const [errorModal, setErrorModel] = useState({
    errorMessage: null,
    openModal: false
  });
  const [successModal, setSuccessModal] = useState(false);

  useEffect(() => {
    async function fetchBallotData() {
      const ballotData = await api.getBallotData();

      if (ballotData) {
        setBallotData({ ...ballotData })
      } else {
        setErrorModel({
          ...errorModal,
          errorMessage: 'Failed to retrieve data',
          openModal: true
        });
      }
    }

    fetchBallotData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const selectNominee = (nominee, category) => {
    const existNomineePerCategory = selectedNomineeList.find((data) => data.category === category);
    if (existNomineePerCategory) {
      setErrorModel({
        ...errorModal,
        errorMessage: 'You have chosen nominee from this category',
        openModal: true
      });

      return;
    }

    setSelectedNomineeList(list => [...list, { ...nominee, category }]);
  }

  const closeErrorModal = () => {
    setErrorModel({
      ...errorModal,
      openModal: false
    });
  }

  const openSuccessModal = () => {
    if (!selectedNomineeList.length) {
      setErrorModel({
        ...errorModal,
        errorMessage: 'You have not choose nominee yet!',
        openModal: true
      });

      return;
    }

    setSuccessModal(true);
  }

  const closeSuccessModal = () => {
    setSuccessModal(false);
    setSelectedNomineeList([]);
  }

  const matchList = (nomineeTitle) => selectedNomineeList.find(nomineeDetail => nomineeDetail.title === nomineeTitle)

  return (
    <div>
      {errorModal.openModal && <ErrorModal errorMsg={errorModal.errorMessage} closeErrorModal={closeErrorModal} />}
      {successModal && <SuccessModal closeSuccessModal={closeSuccessModal} />}
      <div className="max-w-2xl mx-auto py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
        <Title title='AWARDS 2021' />

        {(ballotData && (ballotData.items && ballotData.items.length)) &&
          ballotData.items.map((category) => {
            return (
              <div key={category.id}>
                <Banner title={category.id.replace('-', ' ').toUpperCase()} />

                <div
                  className="relative mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8"
                >
                  {
                    category.items && category.items.length && category.items.map(data => <Card
                      keyId={data.id}
                      matchList={matchList(data.title)}
                      title={data.title}
                      photoUrl={data.photoUrL}
                      onClickButton={() => selectNominee(data, category.id)}
                    />)
                  }
                </div>
              </div>)
          })
        }
      </div>
      <div className='sticky bottom-0 z-2 w-40 h-40 ml-auto'>
        {selectedNomineeList.length !== 0 &&
          <DefaultButton
            title='Submit all selected nominee!'
            onClickButton={openSuccessModal}
          />
        }
      </div>
    </div >
  )
}

export default Ballot;