import DefaultButton from '../Button/Default';

const SuccessModal = ({ closeSuccessModal }) => {
  return (
    <div className="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
      <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div className="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
          <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
            <div className="sm:flex sm:items-start text-center">
              <div className="mt-3 sm:mt-0 sm:ml-0 w-full sm:text-center">
                <h3 className="text-lg leading-6 font-medium text-gray-900" id="modal-title">Your all selected nominee submitted!</h3>
              </div>
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
            <DefaultButton
              title='Close'
              onClickButton={closeSuccessModal}
              className=''
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default SuccessModal