const Title = ({ title }) => {
  return (
    <div className='flex justify-center'>
      <h2 className="text-2xl font-extrabold tracking-tight ">
        {title}
      </h2>
    </div>
  )
}

export default Title;