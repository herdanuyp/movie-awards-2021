const Banner = ({ title }) => {
  return (
    <div className="bg-nominee-background space-y-14 mt-10 sticky top-0 z-1">
      <div className="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-2">
        <div className="flex items-center justify-between flex-wrap">
          <div className="w-0 flex-1 flex items-center">
            <p className="font-medium  truncate">
              {title}
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Banner