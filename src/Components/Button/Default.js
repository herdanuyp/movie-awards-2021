const DefaultButton = ({ onClickButton, title, className }) => {
  return (
    <button
      type="button"
      className={`btn-default btn-default-color ${className}`}
      onClick={onClickButton}
    >
      {title}
    </button>
  )
}

export default DefaultButton;