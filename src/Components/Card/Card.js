import DefaultButton from '../Button/Default';

const Card = ({ keyId, matchList, title, photoUrl, onClickButton }) => {
  return (
    <div
      key={keyId}
      className={`group relative p-3 rounded-md hover:shadow-md hover:border-opacity-0 transform hover:-translate-y-1 transition-all duration-200 ${matchList ? 'selected-list' : 'bg-nominee-background hover:bg-nominee-background-hover'}`}
    >
      <div className='mb-4 text-center min-h-12 h-12'>
        <h3 className={`group-hover:-hover group-hover:text-page-font-hover ${matchList ? 'font-bold' : ''}`}>{title}</h3>
      </div>
      <div className="w-full min-h-96 h-96 h- aspect-w-1 aspect-h-1 overflow-hidden lg:h-96 lg:aspect-none">
        <img src={photoUrl} alt={title} className="w-full h-full object-center object-cover lg:w-full lg:h-full" />
      </div>

      <DefaultButton
        title={matchList ? 'Selected' : 'Select Nominee'}
        onClickButton={onClickButton}
        className={`${matchList ? 'btn-none' : ''}`}
      />
    </div>
  )
}

export default Card;