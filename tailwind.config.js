module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  theme: {
    fontFamily: {
      sans: ['Roboto']
    },
    extend: {
      zIndex: {
        '1': '1',
        '2': '2'
      },
      colors: {
        page: {
          background: {
            DEFAULT: '#0D2436',
            hover: '#009c86'
          },
          font: {
            DEFAULT: '#FFFFFF',
            hover: '#CCCCCC'
          }
        },
        nominee: {
          background: '#009B86',
          selected: '#009B86',
          hover: '#34AC9C',
        }
      }
    },
  },
  plugins: [],
}
